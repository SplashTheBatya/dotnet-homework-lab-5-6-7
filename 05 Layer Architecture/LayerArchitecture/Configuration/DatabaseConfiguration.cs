﻿namespace Booking.WebApi.Configuration;

public class DatabaseConfiguration
{
	public string? ConnectionString { get; set; }
}
